/* globals self */
import Rx from 'rxjs';

import gCounterFactory from './g-counter.logic';

import { FROM_G_COUNTER, TO_G_COUNTER } from './g-counter.protocol.js';

// --- COMMUNICATION ---

const VALID_INCOMING_TYPES = Object.keys(TO_G_COUNTER).reduce((a, k) => {
  a[TO_G_COUNTER[k]] = true; // eslint-disable-line
  return a;
}, {});

const send = (type, payload) => self.postMessage({ type, payload });

// --- ERRORS ---

const throwError = (type, error) =>
  Rx.Observable.throw({
    type,
    payload: error && {
      message: error.message,
      stack: error.stack,
    },
  });

const wrapIfRuntimeError = error =>
  Rx.Observable.if(
    () => error.type, // not ideal
    Rx.Observable.throw(error),
    throwError(FROM_G_COUNTER.ERRORS.RUNTIME, error)
  );

const handleCriticalError = (error) => {
  console.warn('[WORKER] error', error.type, error.payload.message, error.payload.stack);
  send(error.type, error.payload);
  self.close();
};

const unkownError = Rx.Observable.fromEvent(self, 'error')
  .mergeMap(error => throwError(FROM_G_COUNTER.ERRORS.UNKNOWN, error));

// --- LOGIC ---

const messages = Rx.Observable.fromEvent(self, 'message')
  .mergeMap(m => Rx.Observable.if(
    () => m.data && m.data.type && VALID_INCOMING_TYPES[m.data.type],
    Rx.Observable.of(m.data),
    throwError(FROM_G_COUNTER.ERRORS.PROTOCOL_BROKEN, { message: `unknown message type ${m.data && m.data.type}` })
  ));

const createGCounter = payload =>
  Rx.Observable.defer(() => Rx.Observable.of(gCounterFactory()(payload)))
    .catch(error => throwError(FROM_G_COUNTER.ERRORS.LOGIC, error))
    // have to wait for setting up message handler to 100% eliminate race condition
    .mergeMap(gCounter => Rx.Observable.timer(0)
      .do(() => send(FROM_G_COUNTER.INITIALIZED, gCounter.value))
      .mapTo({
        gCounter,
        initPayload: payload,
      }));

const handleInit = messages
  .take(1)
  .mergeMap(({ type, payload }) =>
    Rx.Observable.if(
      () => type !== TO_G_COUNTER.INITIALIZE,
      throwError(FROM_G_COUNTER.ERRORS.PROTOCOL_BROKEN, { message: `uninitialized g-counter received: ${type}` }),
      createGCounter(payload)
    )
  );

const handlers = {
  [TO_G_COUNTER.INCREMENT]: gCounter => () => gCounter.increment(),
  [TO_G_COUNTER.MERGE]: gCounter => payload => gCounter.merge(payload),
};

const handleMessages = gCounter =>
  messages
    .groupBy(m => m.type, m => m.payload)
    .mergeMap(messagesByType =>
      Rx.Observable.if(
        () => !handlers[messagesByType.key],
        throwError(FROM_G_COUNTER.ERRORS.PROTOCOL_BROKEN, { message: `cannot handle ${messagesByType.key}` }),
        messagesByType
          .do(handlers[messagesByType.key](gCounter))
          .catch(error => throwError(FROM_G_COUNTER.ERRORS.LOGIC, error))
      )
    )
    .do(() => send(FROM_G_COUNTER.VALUE, gCounter.value));

// fixme does not belong here - duplicated with main
const randomInt = (min, max) => Math.floor(min + (Math.random() * (max - min)));

const handleUpdatesEmitting = (gCounter, { emitUpdatesMinMs, emitUpdatesMaxMs }) =>
  Rx.Observable.defer(() =>
    Rx.Observable.timer(randomInt(emitUpdatesMinMs, emitUpdatesMaxMs)))
    .do(() => send(FROM_G_COUNTER.UPDATE, gCounter.state))
    .repeat();

const main =
  Rx.Observable.merge(
    unkownError, // may only throw
    handleInit // delivers one value
      .do(x => console.log(`[WORKER] INIT'ED id: ${x.gCounter.id}`)),
  )
    .mergeMap(({ gCounter, initPayload }) =>
      Rx.Observable.merge(
        handleMessages(gCounter),
        handleUpdatesEmitting(gCounter, initPayload)
      ))
    .catch(wrapIfRuntimeError);

main
  .subscribe({
    error: handleCriticalError,
  });

module.exports = {
  TO_G_COUNTER: {
    INITIALIZE: 'initialize', // payload: { size, id }
    INCREMENT: 'increment',
    MERGE: 'merge', // payload: [S_0..S_{size-1}]

    // TERMINATE: 'terminate' // not needed for web workers
  },
  FROM_G_COUNTER: {
    INITIALIZED: 'initialized',
    VALUE: 'value', // payload: current value
    UPDATE: 'update', // payload: { state, sourceId, destinationId }

    // all errors are critical and g_counter should terminate upon an error
    ERRORS: {
      LOGIC: 'logic', // from g-counter logic - most likely invariant violation
      PROTOCOL_BROKEN: 'protocol_broken', // protocol broken
      RUNTIME: 'runtime',
      UNKNOWN: 'unknown', // any other
    },
  },
};

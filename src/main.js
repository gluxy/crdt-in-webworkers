import Rx from 'rxjs';

const GCountWorker = require('worker-loader!./g-counter/g-counter.worker.js'); // eslint-disable-line
const { FROM_G_COUNTER, TO_G_COUNTER } = require('./g-counter/g-counter.protocol.js');

const handleGui = require('./ui/handle-gui.js');
const { ROOT_ID, handleConfig } = require('./config');

// --- COMMUNICATION ---

const VALID_INCOMING_TYPES = Object.keys(FROM_G_COUNTER)
  .reduce((a, k) => {
    a[FROM_G_COUNTER[k]] = true; // eslint-disable-line
    return a;
  }, {});

const workerMessages = worker =>
  Rx.Observable.fromEvent(worker, 'message')
    .mergeMap(m => Rx.Observable.if(
      () => m.data && m.data.type && VALID_INCOMING_TYPES[m.data.type],
      Rx.Observable.of(m.data),
      Rx.Observable.throw(m.data)
    ));

const setupMessages = messages => ({
  values: messages.filter(m => m.type === FROM_G_COUNTER.VALUE)
    .map(m => m.payload),
  updates: messages.filter(m => m.type === FROM_G_COUNTER.UPDATE)
    .map(m => m.payload),
});

// --- LOGIC ---

const waitForInitialized = response =>
  Rx.Observable.if(
    () => response.type === FROM_G_COUNTER.INITIALIZED,
    Rx.Observable.of(response.payload),
    Rx.Observable.throw(new Error('Broken Protocol'))
  );

const createWorkerProxy = ({ id, worker, messages, startValue }) => ({
  id,
  startValue,
  increment: () => worker.postMessage({ type: TO_G_COUNTER.INCREMENT }),
  ...setupMessages(messages),
});

const createWorker = config => (id) => {
  const worker = new GCountWorker();
  worker.postMessage({
    type: TO_G_COUNTER.INITIALIZE,
    payload: Object.assign({ id }, config),
  });
  const messages = workerMessages(worker);
  return messages
    .take(1)
    .mergeMap(waitForInitialized)
    .map(startValue =>
      ({ id, worker, proxy: createWorkerProxy({ id, worker, messages, startValue }) }))
    .do(() => console.log(`[MAIN] INIT'ED id: ${id}`));
};

const createWorkers = config =>
  Rx.Observable.range(0, config.size)
    .mergeMap(createWorker(config))
    .toArray();

const init = handleConfig()
  .mergeMap(config =>
    createWorkers(config)
      .map(workers => ({ workers, config }))
  );

const passUpdateToOther = (other, payload) =>
  other.forEach(w => w.worker.postMessage({
    type: TO_G_COUNTER.MERGE,
    payload,
  }));

// fixme does not belong here - duplicated with g-counter.worker
const randomInt = (min, max) => Math.floor(min + (Math.random() * (max - min)));

const handleUpdates = (workers, { deliverUpdateMinMs, deliverUpdateMaxMs }) =>
  Rx.Observable.from(workers
    .map(({ id, proxy }) =>
      proxy.updates
        .map(payload => [payload, workers.filter(w => w.id !== id)])
        .mergeMap(([payload, other]) =>
          Rx.Observable.of(randomInt(deliverUpdateMinMs, deliverUpdateMaxMs))
            .mergeMap(deliveryTime =>
              Rx.Observable.of({
                sourceId: id,
                deliveryTime,
                payload,
                to: other.map(c => c.id), // ready to support different topologies
              })
                .merge(Rx.Observable.timer(deliveryTime)
                .do(() => passUpdateToOther(other, payload))
                .mergeMapTo(Rx.Observable.empty()))
            )
        )
    )).mergeAll().share(); // share necessary due to side effects

const main = init
  .map(({ workers, config }) => ({
    config,
    updates: handleUpdates(workers, config),
    counters: workers.map(w => w.proxy),
  }))
  .mergeMap(({ updates, counters, config }) =>
    Rx.Observable.merge(
      updates,
      handleGui(ROOT_ID)({ counters, updates, config })
    ))
  .retryWhen(errs => errs
  // should be sent to monitoring and dealt-with asap
    .do(err => console.error('CRITICAL ERROR', err))
    .mergeMapTo(Rx.Observable.empty()));

main.subscribe();
